import React from 'react';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Herois from '../src/screens/Herois'
import HeroiDetalhes from '../src/screens/HeroiDetalhes'
import {
  Text,
  View
} from 'react-native';

const stackNavigation = createStackNavigator()

export default function Navegacao() {

  return (
    <NavigationContainer>
      <stackNavigation.Navigator initialRouteName="Herois">
        <stackNavigation.Screen
          name="Herois"
          component={Herois}
          options={({
            header: () => (
              <View style={{ flexDirection: 'row', margin: 12, justifyContent: 'center' }}>
                <Text style={{ fontWeight: "bold", textDecorationLine: 'underline', color: 'red' }}>BUSCA</Text>
                <Text style={{ fontWeight: "bold", color: 'red' }}> MARVEL </Text>
                <Text style={{ color: 'red' }}>FRONT-END</Text>
              </View>
            )
          })}
        />
        <stackNavigation.Screen
          name="HeroiDetalhes"
          component={HeroiDetalhes}
          options={({ navigation }) => ({
            title: 'Detalhes'
          })} />

      </stackNavigation.Navigator>
    </NavigationContainer>
  );
}