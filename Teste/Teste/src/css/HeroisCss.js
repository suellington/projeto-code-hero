import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    itemLista: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: 'red',
        alignItems: 'center',
        padding: 18
    },
    botoes: {
        padding: 10,
        backgroundColor: 'red',
        borderRadius: 10
    },
    textoBotoes: {
        textAlign: 'center',
        color: 'white'
    }
})