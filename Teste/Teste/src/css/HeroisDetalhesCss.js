import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    backgroundFoto: {
        flex: 4,
        backgroundColor: '#FFF',
        padding: 15
    },
    background: {
        flex: 6,
        backgroundColor: '#FFF',
        padding: 15,
        alignItems: 'center'
    },
    img: {
        width: '100%',
        height: '100%'
    }
})