import React, {useState} from 'react';
import {
  Text,
  ImageBackground,
  View
} from 'react-native';
import HeroiDetalhesCss from '../css/HeroisDetalhesCss'

export default function HeroiDetalhes (props) {

    const { heroi } = props.route.params
    
    return (
        <View style={{ flex: 1 }}>
            <View style={HeroiDetalhesCss.backgroundFoto}>
                <ImageBackground source={{uri: `${heroi.thumbnail.path}.${heroi.thumbnail.extension}`}} style={HeroiDetalhesCss.img}/>                
            </View>
            <View style={HeroiDetalhesCss.background}>
                <Text style={{padding:10, fontSize:20}}>{heroi.name}</Text>
            </View>
        </View>
    )
}