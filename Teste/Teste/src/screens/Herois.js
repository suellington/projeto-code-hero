import React, { useEffect, useState } from 'react';
import {
    Text,
    FlatList,
    TouchableHighlight,
    Image,
    View
} from 'react-native';
import SearchBar from 'react-native-platform-searchbar';
import PaginacaoDot from 'react-native-animated-pagination-dot';
import HeroisCss from '../css/HeroisCss'

export default function Herois(props) {

    const [page, setPage] = useState(0);
    const [dados, setDados] = useState([])
    const [dadosExibidos, setDadosExibidos] = useState([])
    const [busca, setBusca] = useState('')

    const paginar = (indice) => {
        setDadosExibidos(dados.slice(indice, indice + 4));
    }

    const detalhes = async (item) => {
        props.navigation.navigate('HeroiDetalhes', { heroi: item })
    }

    const mostrarPagina = (setPg, pg) => {
        setPage(setPg);
        paginar(pg);
    }

    const filtrarPalavras = (busca) => {
        if (!busca) {
            mostrarPagina(0, 0)
        }
        else {
            const novo = dados.filter(function (item) {
                const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();
                const textData = busca.toUpperCase();
                return itemData.indexOf(textData) > -1;
            })
            setDadosExibidos(novo)
        }
    }

    useEffect(
        () => {
            fetch('http://gateway.marvel.com/v1/public/characters?ts=1611791805&orderBy=name&limit=100&apikey=1bc4a3f5602bfa7c4a95115090475c36&hash=624f820a8582016594f22e2bc8d1e7aa')
                .then((response) => response.json())
                .then((json) => setDados(json.data.results))
                .catch(() => (alert('Erro')))
        }, []
    )
    useEffect(() => {paginar(0) }, [dados])
    useEffect(() => {filtrarPalavras(busca)}, [busca]);

    return (
        <View style={{ flex: 1, }}>
            <View style={{ padding: 10 }}>
                <Text style={{ color: 'red' }}>Nome do Personagem</Text>
                <SearchBar value={busca} onChangeText={busca => setBusca(busca)}/>
            </View>
            <View style={{ backgroundColor: 'red', padding: 12, alignItems: 'center' }}>
                <Text style={{ color: 'white' }}>Nome</Text>
            </View>
            <FlatList
                data={dadosExibidos}
                keyExtractor={item => item.name}
                renderItem={({ item }) => {
                    return (
                        <TouchableHighlight
                            onPress={() => detalhes(item)}>
                            <View style={HeroisCss.itemLista}>
                                <Image style={{ height: 60, width: 60, borderRadius: 30 }} source={{ uri: `${item.thumbnail.path}.${item.thumbnail.extension}` }} />
                                <Text style={{ marginLeft: 10 }}>{item.name}</Text>
                            </View>
                        </TouchableHighlight>
                    );
                }}
            />

            <View style={{ flexDirection: 'row', justifyContent: 'center', padding: 18 }}>
                <TouchableHighlight disabled={page <= 0 ? true : false} style={HeroisCss.botoes}
                    onPress={() => {
                        mostrarPagina(page - 1, (page - 1) * 4)
                    }}>
                    <Text style={HeroisCss.textoBotoes}>
                        voltar
                    </Text>
                </TouchableHighlight>

                <PaginacaoDot
                    activeDotColor={'red'}
                    curPage={page}
                    maxPage={25}
                    sizeRatio={1.0}
                />

                <TouchableHighlight disabled={page >= 25 ? true : false} style={HeroisCss.botoes}
                    onPress={() => {
                        mostrarPagina(page + 1, (page + 1) * 4)
                    }}>
                    <Text style={HeroisCss.textoBotoes}>
                        avançar
                        </Text>
                </TouchableHighlight>
            </View>
        </View>
    )
}